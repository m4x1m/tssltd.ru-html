import CONFIG from './config';

const UnsetWidthHandler = () => {
    CONFIG.header.style.maxWidth = '';
    CONFIG.wrapper.style.maxWidth = '';
};

export default UnsetWidthHandler;


import Swiper from 'swiper/js/swiper';

const ProductsCarousel = () => {
    const PARAMS = {
        carousel   : [...document.querySelectorAll('.js-product-carousel')],
        prev       : '.js-products-carousel-prev',
        next       : '.js-products-carousel-next',
        pagination : '.js-product-carousel-pagination',
        slide      : '.swiper-slide'
    };

    if ( PARAMS.carousel === null )
        return false;

    PARAMS.carousel.forEach((element, index) => {
        let carouselArray = [];
        const slides = element.querySelectorAll(PARAMS.slide).length;

        if ( slides > 4 ) {
            carouselArray[index] = new Swiper(element, {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 0,
                loop: false,
                navigation: {
                    nextEl: PARAMS.next,
                    prevEl: PARAMS.prev,
                },
                pagination: {
                    el        : PARAMS.pagination,
                    clickable : true
                },
                breakpoints: {
                    768 : {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 28,
                    },
                    992 : {
                        slidesPerView: 3,
                        slidesPerGroup: 3,
                        spaceBetween: 28,
                    },
                    1200 : {
                        slidesPerView: 4,
                        slidesPerGroup: 4,
                        spaceBetween: 28,
                    },
                },
                on : {}
            });
        }
    });
};

export default ProductsCarousel;
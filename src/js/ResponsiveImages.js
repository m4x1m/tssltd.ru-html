import CONFIG from "./config";

class ResponsiveImages {
    constructor(data) {
        this.image = [...document.querySelectorAll(data.imageClass)];
        if ( this.image === null )
            return false;

        this.Init();
    }

    ChangeHandler(element, src, srcset) {
        element.setAttribute('src', src);
        element.setAttribute('srcset', srcset);
    }
    ImagesHandler(){
        this.image.forEach(element => {
            const desktopSRC    = element.getAttribute('data-desktop-src');
            const desktopSRCSET = element.getAttribute('data-desktop-srcset');
            const mobileSRC     = element.getAttribute('data-mobile-src');
            const mobileSRCSET  = element.getAttribute('data-mobile-srcset');

            const currentSRC = element.getAttribute('src');

            if ( window.deviceWidth <= CONFIG.tabletXsWidth ) {
                if ( currentSRC !== mobileSRC ) {
                    this.ChangeHandler(element, mobileSRC, mobileSRCSET);
                }
            }
            else {
                if ( currentSRC !== desktopSRC ) {
                    this.ChangeHandler(element, desktopSRC, desktopSRCSET);
                }
            }
        });
    }
    Init(){
        this.ImagesHandler();
    }
};
export default ResponsiveImages;
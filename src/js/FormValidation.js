//https://validatejs.org/
import validate from 'validate.js/validate.min';

const FormValidation = () => {
    const PARAMS = {
        form           : [...document.querySelectorAll('.js-form')],
        inputClass     : 'js-input',
        messagesClass  : 'js-input-messages',
        messageClass   : 'input__message',
        JSMessageClass : 'js-input-message',
        errorClass     : 'input--error',
        successClass   : 'input--success',
        error          : 'error'
    };
    const constraints = {
        USER_EMAIL: {
            presence : true, //required
            email : {
                message: '^ '
            },
            format : {
                pattern : /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
                message: '^Проверьте электоронную почту'
            },
        },
        USER_NAME : {
            presence : true
        },
        WARRANTY_SUBJECT : {
            presence : true
        },
        WARRANTY_COMPANY_NAME : {
            presence : true
        },
        WARRANTY_CERTIFICATE_NO : {
            presence : true
        },
        'TECH_SUPPORT_REQUIRED[]' : function(value, attributes, attr, options, constraints) {
            const checkedBoxes = document.querySelectorAll('input[name="TECH_SUPPORT_REQUIRED[]"]:checked').length > 0;

            return checkedBoxes ? {} : {
                presence : true,
                length: {
                    is: 1,
                    message: "^Выберите хотя бы один продукт"
                }
            };
        }
    };

    if ( PARAMS.form === null )
        return false;

    validate.validators.presence.options = {message: "^Обязательное поле"};

    // Hook up the form so we can prevent it from being posted
    PARAMS.form.forEach(form => {
        form.addEventListener('submit', function(e) {
            if ( !handleFormSubmit(form) ) {
                e.preventDefault();
            }
        });

        // Hook up the inputs to validate on the fly
        const inputs = form.querySelectorAll("input, textarea, select");
        for ( let i = 0; i < inputs.length; ++i ) {
            inputs.item(i).addEventListener("change", function() {
                const errors = validate(form, constraints) || {};

                showErrorsForInput(this, errors[this.name]);
            });
        }
    });

    function handleFormSubmit(form, input) {
        // validate the form against the constraints
        const errors = validate(form, constraints, {fullMessages: false});
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
            return true;
            //showSuccess();
        }
        return false;
    }

    // Updates the inputs with the validation errors
    function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        form.querySelectorAll("input[name], select[name]").forEach(input => {
            // Since the errors can be null if no errors were found we need to handle that
            showErrorsForInput(input, errors && errors[input.name]);
        });
    }

    // Shows the errors for a specific input
    function showErrorsForInput(input, errors) {
        // This is the root of the input
        const formGroup = closestParent(input.parentNode, PARAMS.inputClass);
        // Find where the error messages will be insert into
        const messages = formGroup.querySelector(`.${PARAMS.messagesClass}`);

        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);

        // If we have errors
        if (errors) {
            // we first mark the group has having errors
            formGroup.classList.add(PARAMS.errorClass);
            // then we append all the errors
            errors.forEach(error => {
                addError(messages, error);
            });
        } else {
            // otherwise we simply mark it as success
            formGroup.classList.add(PARAMS.successClass);
        }
    }

    // Recusively finds the closest parent that has the specified class
    function closestParent(child, className) {
        if (!child || child == document) {
            return null;
        }
        if (child.classList.contains(className)) {
            return child;
        } else {
            return closestParent(child.parentNode, className);
        }
    }

    function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove(PARAMS.errorClass);
        formGroup.classList.remove(PARAMS.successClass);
        // and remove any old messages
        formGroup.querySelectorAll(`.${PARAMS.JSMessageClass}.${PARAMS.error}`).forEach(element => {
            element.parentNode.removeChild(element);
        });
    }

    // Adds the specified error with the following markup
    function addError(messages, error) {
        const message = document.createElement('div');
        message.classList.add(PARAMS.messageClass);
        message.classList.add(PARAMS.JSMessageClass);
        message.classList.add(PARAMS.error);
        message.innerText = error;
        messages.appendChild(message);
    }

    function showSuccess() {
        alert('Успешная отправка');
    }
};

export default FormValidation
const CONFIG = {
    developers               : '2020',
    html                     : document.querySelector('html'),
    wrapper                  : document.querySelector('.js-website-wrapper'),
    header                   : document.querySelector('.js-website-header'),
    desktopXsWidth           : 1199,
    tabletWidth              : 991,
    tabletXsWidth            : 767,
    mobileNavExpanded        : '_mobile-navigation-is-expanded',
    quickSearchExpanded      : '_quick-search-is-expanded',
    orderProjectButton       : '_order-project-button-is-visible',
};

export default CONFIG;
import Tabs from '../component/tabs/Tabs';
import Accordion from '../component/accordion/Accordion';
import AccordionTargetable from '../component/accordion/AccordionTargetable';
import ResponsiveImages from './ResponsiveImages';
import ShowMore from '../component/show-more/ShowMore';
import OrderProjectScrollHandler from '../component/order-project/OrderProjectScrollHandler';
import ScrollSection from './ScrollSection';

import HeroSlider from '../component/hero-slider/HeroSlider';
import DefaultSlider from '../component/default-slider/DefaultSlider';
import BrandsCarousel from './BrandsCarousel';
import ProductsCarousel from './ProductsCarousel';
import MobileSlider from '../component/mobile-slider/MobileSlider';
import MobileNavigation from '../component/mobile-navigation/MobileNavigation';
import OsDetect from './OsDetect';
import LoadMore from '../component/load-more/LoadMore';
import CaseSlider from './CaseSlider';
import ScrollTo from './ScrollTo';
import InputTel from './InputTel';
import ImageViewer from '../component/image-viewer/ImageViewer';
import FormValidation from './FormValidation';
import YandexMapInit from '../component/yandex-map/YandexMapInit';
import YoutubePlayer from '../component/youtube-player/YoutubePlayer';
import PopupLauncher from '../component/popup/PopupLauncher';
import Select from '../component/select/Select';
import QuickSearch from '../component/quick-search/QuickSearch';
import ScrollableBlock from '../component/scrollable-block/ScrollableBlock';
import FileUploader from '../component/file-uploader/FileUploader';
import GlightBox from '../component/glightbox/GlightBox';
import ReviewsCarousel from '../component/reviews-carousel/ReviewsCarousel';

window.deviceWidth = document.querySelector('html').clientWidth;
window.addEventListener('resize', ()=> {
    const updatedWidth = document.querySelector('html').clientWidth;
    if ( window.deviceWidth != updatedWidth  ) {
        window.deviceWidth = updatedWidth;
    }
});

OsDetect();
HeroSlider();
BrandsCarousel();
ProductsCarousel();
DefaultSlider('news-events-slider');
DefaultSlider('news-slider');
DefaultSlider('events-slider');
DefaultSlider('webinars-slider');
MobileSlider();
MobileNavigation();
LoadMore();
CaseSlider();
ScrollTo();
InputTel();
FormValidation();
ImageViewer('product-docs');
YandexMapInit();
YoutubePlayer();
PopupLauncher();
Select();
QuickSearch();
ScrollableBlock('.js-scrollable-block');
FileUploader('js-warranty-app-file-uploader');
GlightBox({selector: '.js-photo-glidebox'});
GlightBox({selector: '.js-video-glidebox'});
ReviewsCarousel({carousel: '.js-management-carousel', thumbs: '.js-management-thumbs', pagination: '.js-management-pagination'});
ReviewsCarousel({carousel: '.js-reviews-carousel', thumbs: '.js-reviews-thumbs', pagination: '.js-reviews-pagination'});

new OrderProjectScrollHandler();
new Tabs();
new AccordionTargetable({collapsible : false});
new Accordion({container : '.js-accordions-collapsible', collapsible : true});
new Accordion({container : '.js-accordions-non-collapsible', collapsible : false});
new ResponsiveImages({imageClass: '.js-responsive-image'});
new ShowMore();
new ScrollSection();
//https://supunkavinda.blog/js-detect-outside-click
const OutsideClick = (event, notelem) => {
    let clickedOut = true, i, len = notelem.length;
    for (i = 0;i < len;i++)  {
        if (event.target == notelem[i] || notelem[i].contains(event.target)) {
            clickedOut = false;
        }
    }
    if (clickedOut) return true;
    else return false;
};

export default OutsideClick;
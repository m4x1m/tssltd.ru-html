import CONFIG from './config';

const IosDetect = () => {
    const PARAMS = {
        height : window.screen.height,
        width  : window.screen.width,
        deviceVersion(){
            if (PARAMS.width === 390 && PARAMS.height === 844) {
                return "12";
            }
            if (PARAMS.width === 414 && PARAMS.height === 896) {
                return "Xmax-Xr";
            }
            else if (PARAMS.width === 375 && PARAMS.height === 812) {
                return "X-Xs";
            }
            else if (PARAMS.width === 320 && PARAMS.height === 480) {
                return "4";
            }
            else if (PARAMS.width === 375 && PARAMS.height === 667) {
                return "6";
            }
            else if (PARAMS.width === 414 && PARAMS.height === 736) {
                return "6+";
            }
            else if (PARAMS.width === 320 && PARAMS.height === 568) {
                return "5";
            }
            else if (PARAMS.height <= 480) {
                return "2-3";
            }
            return 'none';
        },
        isIphone() {
            return !!navigator.userAgent.match(/iPhone/i);
        }
    };

    if ( PARAMS.isIphone() ){}
};

export default IosDetect;
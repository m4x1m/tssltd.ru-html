import CONFIG from "./config";

const ClickOutsideHandler = (data) => {
    const PARAMS = {
        element     : data.element,
        marker      : data.marker,
        activeClass : data.activeClass,

        handler() {
            document.addEventListener('click', (e)=>{
                const isClickInside = PARAMS.element.contains(e.target);

                if ( !CONFIG.html.classList.contains(PARAMS.activeClass) )
                    return false;

                if (!isClickInside && PARAMS.marker) {
                    CONFIG.html.classList.remove(PARAMS.activeClass);
                }
            });
        }
    };
    PARAMS.handler();
};

export default ClickOutsideHandler;
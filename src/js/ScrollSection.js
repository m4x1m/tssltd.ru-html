class ScrollSection {
    constructor() {
        this.wrapper = document.getElementById('js-scroll-sections-wrapper');
        if ( this.wrapper === null )
            return false;

        this.scrollNav       = this.wrapper.querySelector('.js-scroll-navigation');
        this.section         = [...document.querySelectorAll('.js-scroll-section')];
        this.link            = [...this.scrollNav.querySelectorAll('.js-scroll-link')];

        this.sectionActiveClass = 'product-section--current';
        this.linkActiveClass    = 'scroll-navigation__element--current';

        this.offsetCorrector = this.scrollNav.clientHeight / 2;
        this.currentSection  = '';

        this.Init();
    }
    GetScrollTop() {
        return window.pageYOffset;
    }
    GetSectionOffsetTop(element) {
        return element.offsetTop;
    }
    GetSectionHeight(element) {
        return element.clientHeight;
    }
    GetSectionBottom(element) {
        return this.GetSectionOffsetTop(element) + this.GetSectionHeight(element);
    }
    LinksHandler(id){
        this.link.forEach(element => {
            element.classList.remove(this.linkActiveClass)
        });
        this.scrollNav.querySelector(`a[href="#${id}"]`).classList.add(this.linkActiveClass);
    }
    BindEvents() {
        document.addEventListener('scroll', ()=>{
            this.section.forEach(section => {
                if ( ( this.GetSectionOffsetTop(section) - this.offsetCorrector ) < this.GetScrollTop() && this.GetSectionBottom(section) > this.GetScrollTop() ) {
                    section.classList.add(this.sectionActiveClass);
                    if ( section.classList.contains(this.sectionActiveClass) ) {
                        this.currentSection = section.getAttribute('id');
                        this.scrollNav.querySelector(`a[href="#${this.currentSection}"]`).classList.add(this.linkActiveClass);
                        this.LinksHandler(this.currentSection);
                    }
                }
                else if ( this.GetSectionBottom(section) < this.GetScrollTop() ) {
                    section.classList.remove(this.sectionActiveClass);
                }
                else {
                    section.classList.remove(this.sectionActiveClass);
                }
            });

        });
    }
    Init() {
        this.BindEvents();
    }

}
export default ScrollSection;
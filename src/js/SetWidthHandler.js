import CONFIG from './config';

const SetWidthHandler = () => {
    CONFIG.header.style.maxWidth = window.deviceWidth + 'px';
    CONFIG.wrapper.style.maxWidth = window.deviceWidth + 'px';
};

export default SetWidthHandler;



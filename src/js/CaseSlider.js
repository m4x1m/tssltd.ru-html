import Swiper from 'swiper/js/swiper';

const CaseSlider = () => {
    const PARAMS = {
        slider     : [...document.querySelectorAll('.js-case-slider')],
        prev       : '.js-case-slider-prev',
        next       : '.js-case-slider-next',
        pagination : '.js-case-slider-pagination',
        slide      : '.swiper-slide',
        initClass  : 'swiper-container-initialized'
    };


    if ( PARAMS.slider === null )
        return false;

    PARAMS.slider.forEach((element, index) => {
        let sliderArray = [];
        const slides    =  element.querySelectorAll(PARAMS.slide).length;

        if ( slides > 1 ) {
            sliderArray[index] = new Swiper(element, {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: false,
                navigation: {
                    nextEl: PARAMS.next,
                    prevEl: PARAMS.prev,
                },
                pagination: {
                    el: PARAMS.pagination,
                    clickable: true
                },
                breakpoints: {
                    768 : {}
                },
                on : {}
            });
        }
    });
};

export default CaseSlider;
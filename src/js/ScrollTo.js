//https://stackoverflow.com/questions/5007530/how-do-i-scroll-to-an-element-using-javascript
//https://perishablepress.com/vanilla-javascript-scroll-anchor/

import smoothscroll from 'smoothscroll-polyfill';

const ScrollTo = () => {

    const PARAMS = {
        link          : [...document.querySelectorAll('.js-scroll-link')],
        distanceToTop(element) {
            return Math.floor(element.offsetTop);
        },
        scrollAnchors(e, element, respond = null) {
            e.preventDefault();

            const ID = (respond) ? respond.getAttribute('href') : element.getAttribute('href');
            const anchor = document.querySelector(ID);

            if (!anchor)
                return;

            const originalTop = PARAMS.distanceToTop(anchor);

            window.scroll({top: originalTop, left: 0, behavior: 'smooth'});
            const checkIfDone = setInterval(function() {
                const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
                if (PARAMS.distanceToTop(anchor) === 0 || atBottom) {
                    clearInterval(checkIfDone);
                }
            }, 100);
        }
    };

    smoothscroll.polyfill();
    PARAMS.link.forEach(element =>{
        element.addEventListener('click', (e)=>{
            PARAMS.scrollAnchors(e, element);
        });
    });
};
export default ScrollTo;
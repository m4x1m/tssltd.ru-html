class PreventPageScroll {
    constructor() {
        this.block = [...document.querySelectorAll('.js-scroll-prevent')];

        this.Init();
    }

    BindEvents(){
        this.block.forEach(element => {
            element.addEventListener('DOMMouseScroll',(e)=>{
                 e.preventDefault();
            });
            element.addEventListener('mousewheel',(e)=>{
                 e.preventDefault();
            });
            element.addEventListener('touchmove',(e)=>{
                 e.preventDefault();
            });
        });
    }
    Init(){
        this.BindEvents()
    }
}

export default PreventPageScroll;
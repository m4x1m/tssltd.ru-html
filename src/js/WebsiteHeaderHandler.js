import CONFIG from "./config";

class WebsiteHeaderHandler {
    constructor() {
        this.html   = CONFIG.html;
        this.header = CONFIG.header;
        if ( this.header == null )
            return false;

        this.shadowClass = '_website-header-has-shadow';

        this.Init();
    }

    getWebsiteHeaderHeight(){
        return this.header.clientHeight
    }
    getScrollTop() {
        return window.pageYOffset;
    }
    AddClassHandler(){
        this.html.classList.add(this.shadowClass);
    }
    RemoveClassHandler(){
        this.html.classList.remove(this.shadowClass);
    }
    BindEvent(){
        window.addEventListener('scroll', ()=>{
            if ( this.getScrollTop() >= this.getWebsiteHeaderHeight() ) {
                this.AddClassHandler();
            }
            else {
                this.RemoveClassHandler();
            }
        });
    }
    Init(){
        this.BindEvent();
    }
}

export default WebsiteHeaderHandler;
import Swiper from 'swiper/js/swiper';

const BrandsCarousel = () => {
    const PARAMS = {
        carousel : document.getElementById('brands-carousel'),
        prev     : '.js-brands-carousel-prev',
        next     : '.js-brands-carousel-next',
    };

    if ( PARAMS.carousel !== null ) {
        PARAMS.offset = parseInt(document.getElementById('brands-carousel').getAttribute('data-offset'));

        const carousel = new Swiper(PARAMS.carousel, {
            slidesPerView: 'auto',
            centeredSlides: true,
            spaceBetween: 16,
            loop: true,
            navigation: {
                nextEl: PARAMS.next,
                prevEl: PARAMS.prev,
            },
            pagination: {},
            breakpoints: {
                768 : {
                    slidesPerView: 3,
                },
                992 : {
                    slidesPerView: 4,
                },
                1200 : {
                    slidesPerView: 'auto',
                    spaceBetween: PARAMS.offset,
                    centeredSlides: false,
                },
            },
            on : {}
        });
    }
};

export default BrandsCarousel;
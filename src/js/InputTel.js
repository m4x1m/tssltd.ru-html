import Inputmask from 'inputmask';
import IOSHandler from './IOSHandler';

const InputTel = () => {
    const PARAMS = {
        phone : [...document.querySelectorAll('.js-input-tel')],

        handler(element) {
            Inputmask({
                mask            : '[+]9 (999) 999-99-99',
                showMaskOnHover : false,
                onKeyValidation: function (key, result) {
                    if ( result.pos === 0 ) {
                        if ( key === 43 || key === 55 || key === 56 ) {
                            element.value = '+7';
                        }
                        else if ( key === 57 ) {
                            element.value = '+7 9';
                        }
                        else {
                            element.value = ''
                        }
                    }
                    if ( result.pos === 1 ) {
                        if ( key === 57 ) {
                            element.value = '+7 9';
                        }
                        else if ( key != 55 ) {
                            element.value = '+7';
                        }
                    }
                },
                onKeyDown: function ( event, buffer, caretPos, opts ) {
                    if ( caretPos.end === 2 && event.keyCode === 8 ) {
                        element.value = '';
                    }
                },
                onBeforeMask : function(value) {
                    if ( IOSHandler() ) {
                        if ( value.length > 4 ) {
                            const reverseVal = value.split('').reverse().join('');
                            let slicedVal;
                            if ( value.length === 11 ) {
                                slicedVal = reverseVal.slice(0, -1);
                            }
                            else if ( value.length === 12 ) {
                                slicedVal = reverseVal.slice(0, -2);
                            }
                            else {
                                slicedVal = reverseVal;
                            }
                            const reversedVal = slicedVal.split('').reverse().join('');
                            const formattedValue = '+7'+reversedVal;

                            return formattedValue;
                        }
                    }
                }
            }).mask(element);
        }
    };

    if ( PARAMS.phone === null )
        return false;

    PARAMS.phone.forEach(element => {
        PARAMS.handler(element);
    });
};

export default InputTel;
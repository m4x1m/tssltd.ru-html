import YandexMap from './YandexMap';

const YandexMapInit = () => {
    const PARAMS = {
        triggerClass : [...document.querySelectorAll('.js-yandex-map-trigger')],
        activeClass  : 'yandex-map--initialized'
    };
    window.APIAdded = false;

    /*if ( PARAMS.triggerClass.length > 0 ) {
        PARAMS.triggerClass.forEach(element => {
            element.addEventListener('click',()=>{
                const id = element.getAttribute('data-yamap-id');
                if ( !document.getElementById(id).classList.contains(PARAMS.activeClass) ) {
                    document.getElementById(id).classList.add(PARAMS.activeClass);
                    YandexMap(id);
                }
            });
        });
    }*/

    if ( document.getElementById('js-contacts-map') != null ) {
        if ( !document.getElementById('js-contacts-map').classList.contains(PARAMS.activeClass) ) {
            document.getElementById('js-contacts-map').classList.add(PARAMS.activeClass);
            YandexMap('js-contacts-map');
        }
    }
    if ( document.getElementById('js-event-map') != null ) {
        if ( !document.getElementById('js-event-map').classList.contains(PARAMS.activeClass) ) {
            document.getElementById('js-event-map').classList.add(PARAMS.activeClass);
            YandexMap('js-event-map');
        }
    }
};

export default YandexMapInit;
import LoadYandexAPI from './LoadYandexAPI';
import FindAncestor from '../../js/FindAncestor';

const YandexMap = (id) => {
    const mapID = document.getElementById(id);
    const parent = FindAncestor(mapID, 'js-yandex-map');

    if ( mapID === null )
        return false;

    const MAP_DATA = {
        coords      : [parseFloat(mapID.getAttribute('data-lat')), parseFloat(mapID.getAttribute('data-lon'))],
        hint        : mapID.getAttribute('data-address'),
        phone       : mapID.getAttribute('data-phone'),
        email       : mapID.getAttribute('data-email'),
        icon        : mapID.getAttribute('data-icon'),
        zoom        : parseInt(mapID.getAttribute('data-zoom')),
        zoomIn      : [...parent.querySelectorAll('.js-ya-maps-zoom-in')],
        zoomOut     : [...parent.querySelectorAll('.js-ya-maps-zoom-out')],
        allowClick  : ( mapID.getAttribute('data-address') !== null ? true : false )
    };


    LoadYandexAPI(()=>{
        ymaps.ready(function () {
            const map = new ymaps.Map(id, {
                center   : MAP_DATA.coords,
                zoom     : MAP_DATA.zoom,
                controls : []
            }),
            balloonLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="yandex-map-balloon js-yandex-map-balloon">' +
                    '<div class="yandex-map-balloon__arrow js-yandex-map-balloon-arrow"></div>' +
                    '<div class="yandex-map-balloon__inside">' +
                        '$[[options.contentLayout observeSize]]' +
                    '</div>' +
                '</div>', {
                    build: function () {
                        this.constructor.superclass.build.call(this);
                        this._$element = document.querySelector('.js-yandex-map-balloon', this.getParentElement());
                        this.applyElementOffset();
                    },
                    clear: function () {
                        this.constructor.superclass.clear.call(this);
                    },
                    onSublayoutSizeChange: function () {
                        balloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);
                        if(!this._isElement(this._$element)) {
                            return;
                        }
                        this.applyElementOffset();
                        this.events.fire('shapechange');
                    },
                    applyElementOffset: function () {},
                    onCloseClick: function (e) {
                        e.preventDefault();
                        this.events.fire('userclose');
                    },
                    getShape: function () {
                        if(!this._isElement(this._$element)) {
                            return balloonLayout.superclass.getShape.call(this);
                        }

                        const position = this._$element.position();

                        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                            [position.left, position.top], [
                                position.left + this._$element[0].offsetWidth,
                                position.top + this._$element[0].offsetHeight + this._$element.querySelector('.js-yandex-map-balloon-arrow')[0].offsetHeight
                            ]
                        ]));
                    },
                    _isElement: function (element) {
                        return element && element[0] && element.querySelector('.js-yandex-map-balloon-arrow')[0];
                    }
                }),
                // Создание вложенного макета содержимого балуна.
                balloonLayoutContent = ymaps.templateLayoutFactory.createClass(
                    '<div class="yandex-map-balloon__element yandex-map-balloon__element--address"><p>$[properties.address]</p></div>' +
                    '<div class="yandex-map-balloon__element yandex-map-balloon__element--phone"><p>$[properties.phone]</p></div>' +
                    '<div class="yandex-map-balloon__element yandex-map-balloon__element--email"><p>$[properties.email]</p></div>'
                ),
                placemark = new ymaps.Placemark(MAP_DATA.coords, {
                        hintContent: MAP_DATA.hint,
                        address : MAP_DATA.hint,
                        phone   : MAP_DATA.phone,
                        email   : MAP_DATA.email
                    },
                    {
                        openBalloonOnClick: MAP_DATA.allowClick, // disables click if no address
                        balloonShadow: false,
                        balloonLayout: balloonLayout,
                        balloonContentLayout: balloonLayoutContent,
                        balloonPanelMaxMapArea: 0,
                        hideIconOnBalloonOpen: false,
                        iconLayout: 'default#image',
                        iconImageHref: MAP_DATA.icon,
                        iconImageSize: [42, 50],
                        iconImageOffset: [-21, -50]
                    });

            const zoomControls = ymaps.templateLayoutFactory.createClass(
                "", {
                    build: function () {
                        zoomControls.superclass.build.call(this);
                        this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                        this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

                        MAP_DATA.zoomIn.forEach(button =>{
                            button.addEventListener('click', this.zoomInCallback);
                        });
                        MAP_DATA.zoomOut.forEach(button =>{
                            button.addEventListener('click', this.zoomOutCallback);
                        });
                    },
                    clear: function () {
                        MAP_DATA.zoomIn.forEach(button =>{
                            button.removeEventListener('click', this.zoomInCallback);
                        });
                        MAP_DATA.zoomOut.forEach(button =>{
                            button.removeEventListener('click', this.zoomOutCallback);
                        });
                        zoomControls.superclass.clear.call(this);
                    },
                    zoomIn: function () {
                        const map = this.getData().control.getMap();
                        map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
                    },
                    zoomOut: function () {
                        const map = this.getData().control.getMap();
                        map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
                    }
                }),
                zoomControl = new ymaps.control.ZoomControl({options: {layout: zoomControls}});

            map.controls.add(zoomControl);
            map.geoObjects.add(placemark);


            map.behaviors.disable('scrollZoom');
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                map.behaviors.disable('drag');
            }
        });
    });
};

export default YandexMap;

const LoadYandexAPI = (callback) => {
    if ( !window.APIAdded ) {
        window.APIAdded = true;

        const script = document.createElement("script");

        //IE
        if (script.readyState){
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        }
        // other browsers
        else {
            script.onload = function(){
                callback();
            };
        }

        script.id    = 'js-yandex-maps-api';
        script.type  = 'text/javascript';
        script.async = true;
        script.src   = '//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1?apikey=d3e505a0-357e-4763-be18-fc7eb56dbced';

        const createdScript = document.getElementsByTagName('script')[0];
        createdScript.parentNode.insertBefore(script, createdScript);
    }
    else {
        callback();
    }
};
export default LoadYandexAPI;
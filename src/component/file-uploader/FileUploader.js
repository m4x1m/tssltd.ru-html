//https://medium.com/typecode/a-strategy-for-handling-multiple-file-uploads-using-javascript-eb00a77e15f
const FileUploader = ( ID ) => {
    const PARAMS = {
        uploader   : document.getElementById(ID),
        element    : '.js-file-uploader-element',
        sizeLimit  : 5,
        errorClass : 'file-uploader--error',

        uploadedFile(file, index) {
            const button = document.createElement('button');
            const buttonName = document.createElement('div');
            button.setAttribute('class', 'file-uploader__element js-file-uploader-element');
            button.setAttribute('type', 'button');
            button.setAttribute('id', `uploaded-file-${index}`);
            button.setAttribute('data-id', index);
            button.setAttribute('data-size', (file.size / (1024*1024)).toFixed(2));
            button.setAttribute('title', file.name);
            buttonName.setAttribute('class', 'file-uploader__element-name');
            button.appendChild(buttonName);
            buttonName.innerText = file.name;
            button.addEventListener('click', PARAMS.removeElement, false);

            return button;
        },
        removeElement(e){
            const element = document.getElementById(e.target.id);
            const ID = parseInt(element.getAttribute('data-id'));
            const size = parseFloat(element.getAttribute('data-size'));

            const parent = element.parentNode;
            const index = Array.prototype.indexOf.call(parent.children, element);


            fileList.splice(index, 1);
            totalSize = totalSize - size;

            if ( PARAMS.errorHandler() ) {
                PARAMS.addErrorClass();
            }
            else {
                PARAMS.removeErrorClass();
            }

            element.remove();
        },
        filesSize(element) {
            return (element.size / (1024*1024)).toFixed(2);
        },
        checkTotalSize() {
            if ( PARAMS.input.files.length ) {
                PARAMS.input.files.forEach(file => {
                    totalSize = totalSize + parseFloat((file.size / (1024*1024)).toFixed(2));
                });

                if ( PARAMS.errorHandler() ) {
                    PARAMS.addErrorClass();
                }
                else {
                    PARAMS.removeErrorClass();
                }
            }
        },
        errorHandler() {
            let error = false;
            if ( totalSize >= PARAMS.sizeLimit ) {
                error = true;
            }
            return error;
        },
        addErrorClass() {
            PARAMS.uploader.classList.add(PARAMS.errorClass);
        },
        removeErrorClass() {
            PARAMS.uploader.classList.remove(PARAMS.errorClass);
        }
    };
    let fileList = [];
    let renderFileList, sendFile;
    let totalSize = 0;


    if ( PARAMS.uploader === null )
        return false;

    PARAMS.trigger  = PARAMS.uploader.querySelector('.js-file-uploader-file');
    PARAMS.input    = PARAMS.uploader.querySelector('.js-file-uploader__input');
    PARAMS.fileList = PARAMS.uploader.querySelector('.js-file-uploader-list');

    PARAMS.trigger.addEventListener('click', ()=>{
        PARAMS.input.click();
    });
    PARAMS.input.addEventListener('change', ()=> {
        for (let i = 0; i < PARAMS.input.files.length; i++) {
            if ( !PARAMS.errorHandler() ) {
                fileList.push(PARAMS.input.files[i]);
            }
        }
        renderFileList();
        PARAMS.checkTotalSize(fileList);
    });

    renderFileList = function () {
        PARAMS.fileList.innerHTML = '';
        fileList.forEach(function (file, index) {
            PARAMS.fileList.appendChild(PARAMS.uploadedFile(file, index));
        });

        console.log(fileList);
    };
};

export default FileUploader;
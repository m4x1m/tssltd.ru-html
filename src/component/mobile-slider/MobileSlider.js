import Swiper from 'swiper/js/swiper';
import CONFIG from  '../../js/config';

const MobileSlider = () => {
    const PARAMS = {
        wrapper   : [...document.querySelectorAll('.js-mobile-slider-wrapper')],
        slider	  : '.js-mobile-slider',
        prev      : '.js-mobile-slider-prev',
        next      : '.js-mobile-slider-next',
        dots      : '.js-mobile-slider-pagination',
        enableCarouse(){
            if ( window.deviceWidth <= CONFIG.tabletXsWidth ) {
                return true;
            }
        },
        destroy(slider) {
            if ( window.deviceWidth > CONFIG.tabletXsWidth ) {
                slider.destroy();
            }
        }
    };
    if ( PARAMS.wrapper !== null ) {
        let sliderArr = [];

        PARAMS.wrapper.forEach((wrapper, index) => {
            const slider = wrapper.querySelector(PARAMS.slider);
            const dots   = wrapper.querySelector(PARAMS.dots);
            const prev   = wrapper.querySelector(PARAMS.prev);
            const next   = wrapper.querySelector(PARAMS.next);

            if ( PARAMS.enableCarouse() ) {
                sliderArr[index] = new Swiper(slider, {
                    slidesPerView	: 1,
                    loop			: false,
                    autoHeight      : false,
                    centeredSlides	: false,
                    spaceBetween    : 0,
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },
                    pagination: {
                        el 		  : dots,
                        clickable : true
                    }
                });
            }

            window.addEventListener('resize',()=> {

            });
        });
    }
};

export default MobileSlider;
class Navigation {
    constructor(){
        this.navigation = document.getElementById('js-navigation');
        if ( this.navigation == null )
            return false;

        this.inside         = this.navigation.querySelector('.js-navigation-inside');
        this.dropdownParent = this.navigation.querySelector('.js-navigation-dropdown-parent');
        this.dropdown       = this.navigation.querySelector('.js-navigation-dropdown');
        this.elementClass   = '.js-navigation-element';
        this.totalSpace     = 0;
        this.breakWidth     = [];

        this.Init()
    }

    CalcTotalWidth(){
        [...this.inside.children].forEach(element => {
            this.totalSpace += element.clientWidth;
            this.breakWidth.push(this.totalSpace);
        });
    }
    //numOfVisibleItems
    CalcElementQuantity(){
        return this.inside.children.length - 1;
    }
    //availableSpace
    CalcAvailableSpace(){
        return this.navigation.clientWidth - this.dropdownParent.clientWidth;
    }
    CalcInsideWidth(){
        return this.inside.clientWidth;
    }
    Check(){
        //requiredSpace
        let numOfVisibleItems   = this.CalcElementQuantity();
        //const requiredSpace     = this.breakWidth[numOfVisibleItems - 1];

        //visible elements
        const visibleElements   = [...this.inside.children];
        const lastElement       = visibleElements[visibleElements.length - 2];

        //hidden elements
        const hiddenElements    = [...this.dropdown.querySelectorAll(this.elementClass)];
        const firstElement      = hiddenElements[0];

        //requiredSpace > this.CalcAvailableSpace()
        if ( this.CalcInsideWidth() > this.CalcAvailableSpace() ) {
            if ( typeof lastElement !== 'undefined' ) {
                this.dropdown.prepend(lastElement);
            }
        }
        //numOfVisibleItems + 1 -- is needed to exclude More element
        else if ( this.CalcAvailableSpace() > this.breakWidth[numOfVisibleItems + 1] ) {
            if ( typeof firstElement !== 'undefined' ) {
                //https://developer.mozilla.org/ru/docs/Web/API/Node/insertBefore
                this.inside.append(firstElement, this.dropdownParent);
            }
        }
    }
    BindEvents(){
        this.CalcTotalWidth();
        this.Check();

        window.addEventListener('resize', ()=> {
            this.CalcAvailableSpace();
            this.Check();
        });
    }
    Init(){
        this.BindEvents()
    }
}

export default Navigation;
import FindAncestor from '../../js/FindAncestor';

class Tabs {
    constructor() {
        this.tabs        = 'js-tabs';
        this.buttonClass = 'js-tabs-button';
        this.paneClass   = 'js-tabs-pane';
        this.buttons     = document.querySelectorAll(`.${this.buttonClass}`);
        this.activeClass = '_active';

        this.init();
    }
    enableTab(element){
        const id  = element.getAttribute('data-tab-id');
        const tab = document.querySelector(`[data-tab="${id}"]`);

        element.classList.add(this.activeClass);
        tab.classList.add(this.activeClass);
    }
    disableTabs(parent){
        const buttons = [...parent.querySelectorAll(`.${this.buttonClass}`)];
        const tabs    = [...parent.querySelectorAll(`.${this.paneClass}`)];
        buttons.forEach(element => {
            element.classList.remove(this.activeClass);
        });
        tabs.forEach(element => {
            element.classList.remove(this.activeClass);
        });
    }
    toggleTabs(){
        const array = [...this.buttons];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = FindAncestor(element, this.tabs);

                this.disableTabs(parent);
                this.enableTab(element);
                this.setURL(element);
            });
        });
    }
    setURL(element) {
        const url = element.getAttribute('data-tab-id');
        window.history.replaceState(undefined, undefined, `#${url}`)
    }
    toggleTabHash() {
        if ( window.location.hash ) {
            if ( document.querySelector(`[data-tab-id="${window.location.hash.substring(1)}"]`) != null ) {
                document.querySelector(`[data-tab-id="${window.location.hash.substring(1)}"]`).click();
                /*setTimeout(()=>{

                }, 350)*/
            }
        }
    }

    init() {
        this.toggleTabs();
        this.toggleTabHash();
    }
}

export default Tabs;
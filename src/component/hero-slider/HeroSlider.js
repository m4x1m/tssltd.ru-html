import Swiper from 'swiper/js/swiper';
import CONFIG from '../../js/config';

const HeroSlider = () => {
    const PARAMS = {
        slider       : document.querySelector('.js-hero-slider'),
        background   : [...document.querySelectorAll('.js-hero-slider-background')],
        prev         : '.js-hero-slider-prev',
        next         : '.js-hero-slider-next',
        pagination   : '.js-hero-slider-pagination',

        changeBackgroundHandler() {
            PARAMS.background.forEach(element => {
                const desktopSRC = element.getAttribute('data-desktop-src');
                const desktopSRCSET = element.getAttribute('data-desktop-srcset');
                const mobileSRC = element.getAttribute('data-mobile-src');
                const mobileSRCSET = element.getAttribute('data-mobile-srcset');

                const currentSRC = element.getAttribute('src');

                if ( window.deviceWidth <= CONFIG.tabletXsWidth ) {
                    if ( currentSRC !== mobileSRC ) {
                        element.setAttribute('src', mobileSRC);
                        element.setAttribute('srcset', mobileSRCSET)
                    }
                }
                else {
                    if ( currentSRC !== desktopSRC ) {
                        element.setAttribute('src', desktopSRC);
                        element.setAttribute('srcset', desktopSRCSET)
                    }
                }
            });
        }
    };

    if ( PARAMS.slider === null )
        return false;

    let speed = 800;
    let delay = 3000;
    if ( PARAMS.slider.getAttribute('data-speed') !== 'undefined' ) {
        speed = parseInt(PARAMS.slider.getAttribute('data-speed'));
    }
    if ( PARAMS.slider.getAttribute('data-delay') !== 'undefined' ) {
        delay = parseInt(PARAMS.slider.getAttribute('data-delay'));
    }

    const slider = new Swiper(PARAMS.slider, {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: false,
        speed: speed,
        autoplay: {
            delay : delay,
            disableOnInteraction : false,
            waitForTransition    : false
        },
        navigation: {
            nextEl: PARAMS.next,
            prevEl: PARAMS.prev,
        },
        pagination: {
            el: PARAMS.pagination,
            clickable: true
        },
        breakpoints: {
            768 : {}
        },
        on : {
            slideChangeTransitionStart : function(){
                setTimeout(function() {
                    slider.animating = false;
                }, 0);
            }
        }
    });

    PARAMS.changeBackgroundHandler();

    window.addEventListener('resize', ()=>{
        PARAMS.changeBackgroundHandler();
    });
};

export default HeroSlider
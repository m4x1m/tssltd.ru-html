import SlimSelect from 'slim-select';
import FindAncestor from '../../js/FindAncestor';
import SimpleBar from "simplebar";

const Select = () => {
    const PARAMS = {
        parentClass      : 'select',
        selectClass      : [...document.querySelectorAll('.js-select')],
        dropdownClass    : '.ss-list',
        scrollBlockClass : '.simplebar-wrapper',

        handler(element) {
            let scrollBar;
            new SlimSelect({
                select      : element,
                placeholder : element.getAttribute('data-placeholder'),
                showSearch  : false,
                beforeOpen: ()=> {
                    //init custom scroll bar
                    const parent = FindAncestor(element, PARAMS.parentClass);
                    const optionsDropdown = parent.querySelector(PARAMS.dropdownClass);

                    scrollBar = new SimpleBar(optionsDropdown, {autoHide: false});
                },
                afterClose: ()=> {
                    //destroy custom scroll bar
                    scrollBar.unMount();
                }
            });
        },
        init() {
            PARAMS.selectClass.forEach(select => {
                PARAMS.handler(select);
            });
        }
    };

    PARAMS.init();
};

export default Select;
import FindAncestor from '../../js/FindAncestor';

class ShowMore {
    constructor () {
        this.more            = 'js-show-more';
        this.buttons         = document.querySelectorAll('.js-show-more-button');
        this.buttonTextClass = '.js-show-more-button-text';
        this.expandedClass   = 'show-more--expanded';

        this.init();
    }

    expandContent(parent){
        if ( !parent.classList.contains(this.expandedClass) ) {
            parent.classList.add(this.expandedClass);
        }
        else {
            parent.classList.remove(this.expandedClass);
        }
    }
    changeText(parent, element){
        const expanded  = element.getAttribute('data-expanded');
        const collapsed = element.getAttribute('data-collapsed');

        if ( !parent.classList.contains(this.expandedClass) ) {
            element.querySelector(this.buttonTextClass).innerText = collapsed;
        }
        else {
            element.querySelector(this.buttonTextClass).innerText = expanded;
        }
    }
    toggleMore() {
        const array = [...this.buttons];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = FindAncestor(element, this.more);

                this.expandContent(parent);
                this.changeText(parent, element);
            });
        });
    }
    init() {
        this.toggleMore();
    }
}

export default ShowMore;
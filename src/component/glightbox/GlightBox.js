import GLightbox from 'glightbox';

const GlightBox = (data) => {
    const PARAMS = {
        selector : data.selector,
        close    : '<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.79561 30.2045L30.2044 9.79564M9.79553 9.79565L30.2044 30.2045" stroke-width="2" stroke-linecap="round"/></svg>',
        prev     : '<svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.5999 25.377L1.82839 13.6077M1.82839 13.6077L13.5999 1.37695M1.82839 13.6077H26.9142" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        next     : '<svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.2284 1.84619L27 13.6154M27 13.6154L15.2284 25.8462M27 13.6154H1.91418" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        html     : `<div id="glightbox-body" class="glightbox-container">
                        <div class="gloader visible"></div>
                        <div class="goverlay"></div>
                            <div class="gcontainer">
                                <div class="gcontainer__inside">
                                    <div id="glightbox-slider" class="gslider"></div>     
                                    <button class="gclose gbtn" tabindex="2" aria-label="Close"><svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.4195 25.141L24.7465 8.81396M8.41943 8.81398L24.7465 25.141" stroke-width="2" stroke-linecap="round"/></svg></button>                    
                                    <button class="gprev gbtn" tabindex="1" aria-label="Предыдщуий" type="button"><svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.5999 25.377L1.82839 13.6077M1.82839 13.6077L13.5999 1.37695M1.82839 13.6077H26.9142" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></button>
                                    <button class="gnext gbtn" tabindex="0" aria-label="Следующий" data-customattribute="example" type="button"><svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.2284 1.84619L27 13.6154M27 13.6154L15.2284 25.8462M27 13.6154H1.91418" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></button>
                                </div> 
                            </div>
                    </div>`
    };

    const GBox = GLightbox({
        selector        : PARAMS.selector,
        touchNavigation : true,
        loop            : false,
        autoplayVideos  : false,
        lightboxHTML    : PARAMS.html
    });
};
export default GlightBox;
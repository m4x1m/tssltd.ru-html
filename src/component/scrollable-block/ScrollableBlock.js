//https://github.com/Grsmto/simplebar/tree/master/packages/simplebar
import SimpleBar from 'simplebar';

const ScrollableBlock = (id) => {
    const PARAMS = {
        block : [...document.querySelectorAll(id)],

        init(){
            PARAMS.block.forEach(element => {
                new SimpleBar(element, {autoHide: false});
            });
        }
    };

    PARAMS.init();
};

export default ScrollableBlock;
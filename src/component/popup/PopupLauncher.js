import Popup from './Popup';

const PopupLauncher = () => {
    const PARAMS = {
        trigger : [...document.querySelectorAll('.js-popup-launcher')]
    };

    PARAMS.trigger.forEach(element => {
        const button = element.getAttribute('data-popup-trigger');
        const popup  = element.getAttribute('data-popup-id');


        if ( document.getElementById(popup) === null && typeof button != 'undefined' )
            return false;

        Popup({id: popup, trigger: button});
    });
};

export default PopupLauncher;
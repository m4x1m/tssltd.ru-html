//https://rmodal.js.org/
import RModal from '../../../node_modules/rmodal/dist/rmodal';
import SetWidthHandler from '../../js/SetWidthHandler';
import UnsetWidthHandler from '../../js/UnsetWidthHandler';

const Popup = (data) => {
    if ( document.getElementById(data.id) === null )
        return false;

    const PARAMS = {
        button : [...document.querySelectorAll('[data-popup-trigger="'+ data.trigger +'"]')],
        popup  : document.getElementById(data.id),
        close  : [...document.querySelectorAll('.js-popup-close')]
    };

    const popup = new RModal(PARAMS.popup, {
        escapeClose: true,
        closeTimeout: 0,
        focus: false,
        beforeOpen: function(next) {
            next();
            SetWidthHandler();
        },
        afterOpen: function() {},
        beforeClose: function(next) {
            next();
        },
        afterClose: function() {
            UnsetWidthHandler();
        },
        bodyClass: 'popup--is-opened',
        dialogClass: 'popup__container',
        //dialogOpenClass: 'animated fadeIn',
        //dialogCloseClass: 'animated fadeOut',
        //focus: true,
        //focusElements: ['input.form-control', 'textarea', 'button.btn-primary']
    });

    document.addEventListener('keydown', (e)=> {
        popup.keydown(e);
    }, false);
    PARAMS.button.forEach(button => {
        button.addEventListener('click', (e)=> {
            e.preventDefault();
            popup.open();
        }, false);
    });
    PARAMS.close.forEach(close => {
        close.addEventListener('click', (e)=> {
            e.preventDefault();

            popup.close();
        })
    });

    window.popup = popup;
};

export default Popup;
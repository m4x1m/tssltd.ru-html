import CONFIG from '../../js/config';
import FocusAndOpenKeyboard from '../../js/FocusAndOpenKeyboard';
import ClickOutsideHandler from '../../js/ClickOutsideHandler';

const QuickSearch = () => {
    const PARAMS = {
        search : document.getElementById('quick-search'),
        input  : document.getElementById('quick-search').querySelector('input'),
        button : [...document.querySelectorAll('.js-quick-search-button')],
        expandHandler(){
            CONFIG.html.classList.add( CONFIG.quickSearchExpanded );
        },
        collapseHandler(){
            CONFIG.html.classList.remove( CONFIG.quickSearchExpanded );
        },
        handler(){
            if ( !CONFIG.html.classList.contains( CONFIG.quickSearchExpanded ) ) {
                PARAMS.expandHandler();
                FocusAndOpenKeyboard(PARAMS.input, 150);
            }
            else {
                PARAMS.collapseHandler();
            }
        }
    };

    PARAMS.button.forEach(button =>{
        button.addEventListener('click', ()=>{
            PARAMS.handler();
        });
    });

    ClickOutsideHandler({element : PARAMS.search, marker : true, activeClass : CONFIG.quickSearchExpanded});
};

export default QuickSearch;
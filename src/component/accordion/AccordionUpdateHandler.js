const AccordionUpdateHandler = (element) => {
    const PARAMS = {
        bodyClass   : '.js-accordion-body',
        insideClass : '.js-accordion-inside',
        updateHandler(element) {
            element.querySelector(PARAMS.bodyClass).style.height = `${element.querySelector(PARAMS.insideClass).clientHeight}px`;
        }
    };

    PARAMS.updateHandler(element);
}

export default AccordionUpdateHandler;
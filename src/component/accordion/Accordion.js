import CONFIG from '../../js/config';

class Accordion {
    constructor(data) {
        this.container     = [...document.querySelectorAll(data.container)];
        this.accrodion     = '.js-accordion';
        this.buttonClass   = '.js-accordion-button';
        this.bodyClass     = '.js-accordion-body';
        this.insideClass   = '.js-accordion-inside';
        this.hiddenClass   = 'accordion--is-hidden';
        this.expandedClass = 'accordion--is-expanded';
        this.collapsible   =  data.collapsible;

        this.Init();
    }

    ExpandHandler(element){
        if ( this.collapsible ) {
            this.container.forEach(container => {
                const accordion = [...container.querySelectorAll(this.accrodion)];

                accordion.forEach(accordion => {
                    accordion.classList.remove(this.expandedClass);
                    accordion.querySelector(this.bodyClass).style.height = 0;
                });
            });
        }

        element.classList.add(this.expandedClass);
        element.querySelector(this.bodyClass).style.height = `${this.GetContentHeight(element)}px`;
    }
    CollapseHandler(element){
        element.classList.remove(this.expandedClass);
        requestAnimationFrame(() => element.querySelector(this.bodyClass).style.height = 0)
    }
    GetContentHeight(element){
        return element.querySelector(this.insideClass).clientHeight;
    }
    GetAccordionReady(element){
        if ( !element.classList.contains(this.hiddenClass) ) {
            element.querySelector(this.bodyClass).style.height = `${this.GetContentHeight(element)}px`;
        }
        else {
            element.querySelector(this.bodyClass).style.height = 0;
            element.classList.remove(this.hiddenClass);
        }
    }
    BindEvents(){
        this.container.forEach(container => {
            const accordion = [...container.querySelectorAll(this.accrodion)];
            accordion.forEach(accordion => {
                const isMobile = accordion.getAttribute('data-mobile');
                const button = accordion.querySelector(this.buttonClass);

                if ( typeof isMobile !== 'undefined' ) {
                    if ( window.deviceWidth <= CONFIG.tabletXsWidth ) {
                        this.GetAccordionReady(accordion);
                    }
                }
                else {
                    this.GetAccordionReady(accordion);
                }
                button.addEventListener('click', (e)=>{
                    //if mobile do nothing at desktop
                    if ( isMobile != null && window.deviceWidth > CONFIG.tabletXsWidth ) {
                        return false;
                    }
                    else {
                        e.preventDefault();
                        if ( !accordion.classList.contains(this.expandedClass) ) {
                            this.ExpandHandler(accordion);
                        }
                        else {
                            this.CollapseHandler(accordion);
                        }
                    }
                });
            });
        });

        window.addEventListener('resize', ()=>{
            this.container.forEach(container => {
                const accordion = [...container.querySelectorAll(this.accrodion)];
                accordion.forEach(accordion => {
                    const isMobile = accordion.getAttribute('data-mobile');

                    if ( isMobile !== null ) {
                        if ( window.deviceWidth > CONFIG.tabletXsWidth ) {
                            accordion.classList.remove(this.expandedClass);
                            accordion.querySelector(this.bodyClass).style.height = '';
                        }
                        else {
                            this.CollapseHandler(accordion);
                        }
                    }
                });
            });
        });
    }
    Init(){
        this.BindEvents();
    }
}

export default Accordion;
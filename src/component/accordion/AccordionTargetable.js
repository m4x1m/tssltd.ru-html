class AccordionTargetable {
    constructor(data) {
        this.accordion     = [...document.querySelectorAll('.js-accordion-targetable')];
        this.buttonClass   = '.js-accordion-targetable-button';
        this.bodyClass     = '.js-accordion-targetable-body';
        this.insideClass   = '.js-accordion-targetable-inside';
        this.hiddenClass   = 'accordion--is-hidden';
        this.expandedClass = 'accordion--is-expanded';
        this.collapsible   =  data.collapsible;

        this.Init();
    }

    ExpandHandler(element){
        if ( this.collapsible ) {
            this.accordion.forEach(accordion => {
                accordion.classList.remove(this.expandedClass);
                accordion.querySelector(this.bodyClass).style.height = 0;
            });
        }

        element.classList.add(this.expandedClass);
        element.querySelector(this.bodyClass).style.height = `${this.GetContentHeight(element)}px`;
    }
    CollapseHandler(element){
        element.classList.remove(this.expandedClass);
        requestAnimationFrame(() => element.querySelector(this.bodyClass).style.height = 0)
    }
    GetContentHeight(element){
        return element.querySelector(this.insideClass).clientHeight;
    }
    GetAccordionReady(element){
        if ( !element.classList.contains(this.hiddenClass) ) {
            element.querySelector(this.bodyClass).style.height = `${this.GetContentHeight(element)}px`;
        }
        else {
            element.querySelector(this.bodyClass).style.height = 0;
            element.classList.remove(this.hiddenClass);
        }
    }
    BindEvents(){
        this.accordion.forEach(accordion => {
            const button = accordion.querySelector(this.buttonClass);

            this.GetAccordionReady(accordion);

            button.addEventListener('click', (e)=>{
                const ID = e.target.getAttribute('data-accordion-id');
                const currentAccordion = document.getElementById(ID);

                if ( !currentAccordion.classList.contains(this.expandedClass) ) {
                    this.ExpandHandler(currentAccordion);
                }
                else {
                    this.CollapseHandler(currentAccordion);
                }
            });
        });
    }
    Init(){
        this.BindEvents();
    }
};

export default AccordionTargetable;
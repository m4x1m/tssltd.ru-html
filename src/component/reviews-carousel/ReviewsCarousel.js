import Swiper from 'swiper/js/swiper';

const ReviewsCarousel = (data) => {
    const PARAMS = {
        carousel    : document.querySelector(data.carousel),
        thumbs      : document.querySelector(data.thumbs),
        pagination  : document.querySelector(data.pagination)
    };

    if ( PARAMS.carousel === null )
        return false;

    const thumbs = new Swiper(PARAMS.thumbs, {
        direction             : 'vertical',
        slidesPerView         : 'auto',
        loop                  : false,
        freeMode              : true,
        freeModeSticky        : true,
        watchSlidesVisibility : true,
        watchSlidesProgress   : true
    });
    const carousel = new Swiper(PARAMS.carousel, {
        slidesPerView : 1,
        loop          : false,
        thumbs : {
            swiper: thumbs
        },
        pagination  : {
            el: PARAMS.pagination,
            clickable : true
        },
    });
};

export default ReviewsCarousel;
import Swiper from 'swiper/js/swiper';
import CONFIG from '../../js/config';

const DefaultSlider = ( id ) => {
    const PARAMS = {
        slider              : document.getElementById(id),
        slideClass          : '.js-default-slider-element',
        prevClass           : '.js-default-slider-prev',
        nextClass           : '.js-default-slider-next',
        dots                : '.js-default-slider-pagination',
        sliderTrigger       : '.js-default-slider-updater',
        initClass           : 'swiper-container-initialized',
        nonDestroyableClass : 'default-slider--non-destroyable'
    };

    if ( PARAMS.slider !== null ) {
        PARAMS.slide  = document.getElementById(id).querySelectorAll(PARAMS.slideClass);
        PARAMS.prev   = document.getElementById(id).querySelector(PARAMS.prevClass);
        PARAMS.next   = document.getElementById(id).querySelector(PARAMS.nextClass);
        PARAMS.offset = parseInt(document.getElementById(id).getAttribute('data-slides-offset'));


        if ( PARAMS.slide.length > 3 ) {
            const slider = new Swiper(PARAMS.slider, {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: false,
                navigation: {
                    nextEl: PARAMS.next,
                    prevEl: PARAMS.prev,
                },
                pagination: {
                    el: PARAMS.dots,
                    clickable: true
                },
                breakpoints: {
                    768 : {
                        slidesPerView: 3,
                        spaceBetween: PARAMS.offset,
                    }
                },
                on : {}
            });

            document.querySelectorAll(PARAMS.sliderTrigger).forEach(element => {
                element.addEventListener('click', ()=>{
                    if ( window.deviceWidth > CONFIG.tabletXsWidth ) {
                        setTimeout(()=>{
                            slider.update();
                        }, 0);
                    }
                });
            });

            if ( window.deviceWidth <= CONFIG.tabletXsWidth && !PARAMS.slider.classList.contains(PARAMS.nonDestroyableClass) ) {
                if ( PARAMS.slider.classList.contains(PARAMS.initClass) ) {
                    slider.destroy();
                }
            }
        }
    }
};

export default DefaultSlider;
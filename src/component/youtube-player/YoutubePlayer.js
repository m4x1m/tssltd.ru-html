import YouTubePlayer from 'youtube-player';
import FindAncestor from '../../js/FindAncestor';

const YoutubePlayer = () => {
    let player;

    const PARAMS = {
        player      : 'js-youtube-player',
        button      : [...document.querySelectorAll('.js-youtube-button')],
        container   : '.js-youtube-player-container',
        activeClass : 'youtube-player--is-playing',

        handler(element, e) {
            const parent = FindAncestor(element, PARAMS.player);
            const container = parent.querySelector(PARAMS.container);
            const ID = element.getAttribute('data-video-id');

            parent.classList.add(PARAMS.activeClass);

            if ( container.querySelector(`iframe${PARAMS.container}`) === null ) {
                player = YouTubePlayer(container, {
                    height : '100%',
                    width  : '100%',
                    playerVars: {
                        rel      : 0,
                        controls : 1,
                        showinfo : 0,
                        autoplay : 1
                    }
                });
                player.loadVideoById(ID);
                PARAMS.startVideo(player);

                player.on('stateChange', (e) => {
                    PARAMS.onPlayerStateChange(parent, e)
                });
            }

        },
        startVideo(player) {
            player.playVideo();
        },
        onPlayerStateChange(parent,  e) {
            if(e.data === 0) {
                setTimeout(function(){
                    parent.classList.remove(PARAMS.activeClass);
                    player.destroy();
                }, 200);
            }
        }
    };

    PARAMS.button.forEach(button => {
        button.addEventListener('click', (e)=>{
            e.preventDefault();

            PARAMS.handler(button, e);
        })
    });
};
export default YoutubePlayer;
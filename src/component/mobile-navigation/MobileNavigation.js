import CONFIG from '../../js/config';
import ClickOutsideHandler from '../../js/ClickOutsideHandler';

const MobileNavigation = () => {
    const PARAMS = {
        nav    : document.getElementById('mobile-navigation'),
        button : document.getElementById('mobile-navigation-button'),

        expandHandler(){
            CONFIG.html.classList.add(CONFIG.mobileNavExpanded);

            if ( CONFIG.html.classList.contains( CONFIG.quickSearchExpanded ) ) {
                CONFIG.html.classList.remove( CONFIG.quickSearchExpanded )
            }
        },
        collapseHandler(){
            CONFIG.html.classList.remove(CONFIG.mobileNavExpanded);
        }
    };

    if ( PARAMS.button === null )
        return false;


    PARAMS.button.addEventListener('click', (e)=>{
        if ( !CONFIG.html.classList.contains(CONFIG.mobileNavExpanded) ) {
            e.stopPropagation();
            PARAMS.expandHandler();
        }
        else {
            PARAMS.collapseHandler();
        }
    });
    ClickOutsideHandler({element : PARAMS.nav, marker : true, activeClass : CONFIG.mobileNavExpanded});
};
export default MobileNavigation;
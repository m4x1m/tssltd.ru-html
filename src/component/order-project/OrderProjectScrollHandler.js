import CONFIG from '../../js/config'

class OrderProjectScrollHandler {
    constructor() {
        this.form = document.getElementById('order-project');
        if ( this.form === null )
            return false;

        this.anchor = document.getElementById('order-project-button-anchor');

        this.Init();
    }

    getScreenHeight(){
        return window.innerHeight;
    }
    getOffsetTop() {
        return window.pageYOffset;
    }
    getAnchorHeight(){
        return this.anchor.clientHeight;
    }
    getAnchorOffsetTop(){
        return this.anchor.offsetTop;
    }
    getAnchorBottom(){
        return this.getAnchorOffsetTop() + this.getAnchorHeight();
    }
    getButtonVisible() {
        if ( !CONFIG.html.classList.contains( CONFIG.orderProjectButton ) ) {
            CONFIG.html.classList.add( CONFIG.orderProjectButton )
        }
    }
    getButtonHidden() {
        if ( CONFIG.html.classList.contains( CONFIG.orderProjectButton ) ) {
            CONFIG.html.classList.remove( CONFIG.orderProjectButton )
        }
    }
    getFormOffsetTop(){
        return this.form.offsetTop;
    }
    BindEvents(){
        document.addEventListener('scroll', ()=>{
            if ( this.getOffsetTop() > this.getAnchorBottom() && (this.getFormOffsetTop() - this.getScreenHeight()) > this.getOffsetTop() ) {
                this.getButtonVisible();
            }
            else if ( (this.getFormOffsetTop() - this.getScreenHeight()) < this.getOffsetTop() ) {
                this.getButtonHidden();
            }
            else {
                this.getButtonHidden();
            }
        });
    }
    Init(){
        this.BindEvents();
    }
}

export default OrderProjectScrollHandler;
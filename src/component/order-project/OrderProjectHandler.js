import CONFIG from '../../js/config';

const OrderProjectHandler = () => {
    const PARAMS = {
        form  : document.getElementById('order-project'),
        open  : [...document.querySelectorAll('.js-order-project-open')],
        close : [...document.querySelectorAll('.js-order-project-close')],
        expandedClass : '_order-project-is-expanded',

        expandHandler(){
            if ( !CONFIG.html.classList.contains(PARAMS.expandedClass) ) {
                CONFIG.html.classList.add(PARAMS.expandedClass)
            }
        },
        collapseHandler(){
            if ( CONFIG.html.classList.contains(PARAMS.expandedClass) ) {
                CONFIG.html.classList.remove(PARAMS.expandedClass)
            }
        },
    };

    if ( PARAMS.form === null )
        return false;

    PARAMS.open.forEach(element => {
        element.addEventListener('click', ()=>{
            PARAMS.expandHandler();
        });
    });
    PARAMS.close.forEach(element => {
        element.addEventListener('click', ()=>{
            PARAMS.collapseHandler();
        });
    });
};
export default OrderProjectHandler;
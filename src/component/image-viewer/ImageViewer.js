//https://fengyuanchen.github.io/viewerjs/
import Viewer from 'viewerjs';

const ImageViewer = (id) => {
    const PARAMS = {
        gallery : document.getElementById(id)
    };

    if ( PARAMS.gallery === null )
        return false;

    PARAMS.navBar = parseInt(document.getElementById(id).getAttribute('data-nav-bar'));

    const gallery = new Viewer(PARAMS.gallery, {
        navbar     : PARAMS.navBar,
        title      : false,
        toolbar    : true,
        transition : false,
        zoomRatio  : 0.2,
        url        : 'data-original',
        show() {},
        hidden() {},
        viewed() {},
    });
};

export default ImageViewer;
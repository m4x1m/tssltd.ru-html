import FindAncestor from '../../js/FindAncestor';

const LoadMore = () => {
    const PARAMS = {
        parent            : 'js-load-more',
        element           : '.js-load-more-element',
        elementMobile     : '.js-load-more-element-mobile',
        buttons           : document.getElementsByClassName('js-load-more-button'),
        text              : '.js-load-more-button-text',
        expandedClass     : 'load-more--expanded',
        hiddenClass       : 'load-more__element--hidden',
        mobileHiddenClass : 'load-more__element--mobile-hidden',
        bindEvent : ()=> {
            for ( let button of PARAMS.buttons ) {
                button.addEventListener('click', ()=>{
                    const parent = PARAMS.findAncestor(button, PARAMS.parent);
                    const hideable = parent.getAttribute('data-hideable');

                    PARAMS.toggleHandler(parent, button, hideable);
                });
            }
        },
        toggleHandler : (parent, button, hideable)=> {
            const elements       = parent.querySelectorAll(PARAMS.element);
            const elementsMobile = parent.querySelectorAll(PARAMS.elementMobile);
            if ( hideable === null ) {
                if ( !parent.classList.contains(PARAMS.expandedClass) ) {
                    parent.classList.add(PARAMS.expandedClass);
                    PARAMS.changeTextHandler(button, button.getAttribute('data-expanded'));

                    elements.forEach(element => {
                        if ( element.classList.contains(PARAMS.hiddenClass) ) {
                            element.classList.remove(PARAMS.hiddenClass);
                        }
                    });
                    elementsMobile.forEach(element => {
                        if ( element.classList.contains(PARAMS.mobileHiddenClass) ) {
                            element.classList.remove(PARAMS.mobileHiddenClass);
                        }
                    });
                }
                else {
                    parent.classList.remove(PARAMS.expandedClass);
                    PARAMS.changeTextHandler(button, button.getAttribute('data-collapsed'));

                    elements.forEach(element => {
                        if ( !element.classList.contains(PARAMS.hiddenClass) ) {
                            element.classList.add(PARAMS.hiddenClass);
                        }


                    });
                    elementsMobile.forEach(element => {
                        if ( !element.classList.contains(PARAMS.mobileHiddenClass) ) {
                            element.classList.remove(PARAMS.hiddenClass);
                            element.classList.add(PARAMS.mobileHiddenClass);
                        }
                    });
                }
            }
            else {
                elements.forEach(element => {
                    if (element.classList.contains(PARAMS.hiddenClass) ) {
                        element.classList.remove(PARAMS.hiddenClass);
                    }
                });
                elementsMobile.forEach(element => {
                    if ( element.classList.contains(PARAMS.mobileHiddenClass) ) {
                        element.classList.remove(PARAMS.mobileHiddenClass);
                    }
                });
                button.setAttribute('hidden', true);
            }
        },
        changeTextHandler(element, text){
            element.querySelector(PARAMS.text).innerHTML = text;
        },
        findAncestor : (el, cls) => {
            while ((el = el.parentElement) && !el.classList.contains(cls));
            return el;
        }
    };

    PARAMS.bindEvent();
};

export default LoadMore;
